(ns hwo2014bot.state_test
  (:use
   [midje sweet]
   [hwo2014bot state messages]))

(facts "about yourCar message"
  (fact "stores car information to state"
    (let [state (atom {})]
      (update-state your-car-message state)
      (:yourCar @state) => (:data your-car-message))))

(facts "about carPositions message"
  (fact "stores current location in track to state"
    (let [state (atom {:yourCar (:data your-car-message)})]
      (update-state car-positions-message-a state)
      (:carPosition @state) => (select-keys ((:data car-positions-message-a) 1) [:angle :piecePosition])
      (update-state car-positions-message-b state)
      (:carPosition @state) => (select-keys ((:data car-positions-message-b) 0) [:angle :piecePosition]))))

(facts "about gameInit message"
  (fact "stores whole gameInit message to state"
    (let [state (atom {})]
      (update-state game-init-message state)
      (:race @state) => (:race (:data game-init-message)))))

(ns hwo2014bot.bot_test
  (:use
   [midje sweet]
   [hwo2014bot bot messages]))

(facts "about gameInit message"
  (fact "associates race information to state"
    (handle-msg game-init-message (atom {})) => {:msgType "ping" :data "ping"}))

(facts "about carPositions message"
  (fact "always throttles with 0.5"
    (handle-msg car-positions-message-a (atom {:yourCar (:data your-car-message)})) => {:msgType "throttle" :data 0.5}))

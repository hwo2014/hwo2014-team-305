(ns hwo2014bot.messages)

(def game-init-message
  {:msgType "gameInit"
   :data {:race {:track {:id "trackId"
                         :name "Test Track"
                         :pieces [{:length 100.0}
                                  {:length 100.0, :switch true}
                                  {:radius 100, :angle 45.0}
                                  {:radius 200, :angle 22.5, :switch true}
                                  {:length 80.0}]
                         :lanes [{:distanceFromCenter -10 :index 0}
                                 {:distanceFromCenter 10 :index 1}]
                         :startingPoint {:position {:x -300.0, :y -44.0} :angle 90.0}}
                 :cars [{:id {:name "Trio Petokeksi" :color "pink"}
                         :dimensions {:length 40.0 :width 20.0 :guideFlagPosition 10.0}}
                        {:id {:name "Master of Otamatone" :color "red"}
                         :dimensions {:length 40.0 :width 20.0 :guideFlagPosition 10.0}}]
                 :raceSession {:laps 3 :maxLapTimeMs 60000 :quickRace true}}}
   :gameId "9397da2e-525f-4ae1-866f-d823df654d79"})

(def car-positions-message-a
  {:msgType "carPositions"
   :data [{:id {:name "Trio Petokeksi", :color "pink"}
           :angle 0.0
           :piecePosition {:pieceIndex 0
                           :inPieceDistance 3.191236485738495
                           :lane {:startLaneIndex 1 :endLaneIndex 1}
                           :lap 0}}
          {:id {:name "Master of Otamatone", :color "red"}
           :angle 0.0
           :piecePosition {:pieceIndex 0
                           :inPieceDistance 4.187434789212357
                           :lane {:startLaneIndex 0 :endLaneIndex 0}
                           :lap 0}}]
   :gameId "9397da2e-525f-4ae1-866f-d823df654d79"
   :gameTick 1})

(def car-positions-message-b
  {:msgType "carPositions"
   :data [{:id {:name "Master of Otamatone", :color "red"}
           :angle 0.0
           :piecePosition {:pieceIndex 1
                           :inPieceDistance 7.295136848686974
                           :lane {:startLaneIndex 0 :endLaneIndex 0}
                           :lap 0}}
          {:id {:name "Trio Petokeksi", :color "pink"}
           :angle 0.0
           :piecePosition {:pieceIndex 1
                           :inPieceDistance 7.342510231239952
                           :lane {:startLaneIndex 1 :endLaneIndex 1}
                           :lap 0}}]
   :gameId "9397da2e-525f-4ae1-866f-d823df654d79"
   :gameTick 1})

(def your-car-message
  {:msgType "yourCar"
   :data {:name "Master of Otamatone" :color "red"}
   :gameId "9397da2e-525f-4ae1-866f-d823df654d79"})

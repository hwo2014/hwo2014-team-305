(ns hwo2014bot.bot
  (:use
   [hwo2014bot state]))

(def ping-message
  {:msgType "ping" :data "ping"})

(defmulti handle-msg
  (fn [msg state]
    (update-state msg state)
    (:msgType msg)))

(defmethod handle-msg "carPositions" [msg state]
  {:msgType "throttle" :data 0.5})

(defmethod handle-msg "gameInit" [msg state]
  ping-message)

(defmethod handle-msg :default [msg state]
  ping-message)

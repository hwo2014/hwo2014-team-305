(ns hwo2014bot.state
  (:use
   [hwo2014bot utils]))

(defn- get-own-car-index [msg state]
  (let [own-car-name (:name (:yourCar @state))
        car-positions (:data msg)
        get-fn (fn [i list] (:name (:id (list i))))]
    (index-of own-car-name car-positions get-fn)))

(defmulti update-state :msgType)

(defmethod update-state "carPositions" [msg state]
  (let [own-car-index (get-own-car-index msg state)
        car-position (select-keys ((:data msg) own-car-index) [:angle :piecePosition])]
    (swap! state (fn [state] (merge state {:carPosition car-position})))))

(defmethod update-state "gameInit" [msg state]
  (let [race (:data msg)]
    (swap! state (fn [state] (merge state race)))))

(defmethod update-state "yourCar" [msg state]
  (let [your-car (:data msg)]
    (swap! state (fn [state] (merge state {:yourCar your-car})))))

(defmethod update-state :default [msg state])

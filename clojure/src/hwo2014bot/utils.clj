(ns hwo2014bot.utils)

(defn index-of [var list get-fn]
  ((fn [i]
     (let [candidate (get-fn i list)]
       (if
         (= candidate var)
         i
         (recur (inc i))))) 0))
